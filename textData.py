helpText = {
	"help": "Show this message",
	"voice voicename": "Change your voice to voicename"
}

errorEmptyMessage = "Please give me something to say as well!"
errorCantGetVoicechannel = "I can't see on which voice channel you are!"
errorNotInAnyVoiceChannel = "I'm not in any voice channel."