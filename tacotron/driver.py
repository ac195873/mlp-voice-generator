import sys, os

def getPath ():
	return os.path.dirname (os.path.realpath (__file__))

sys.path.append(getPath () + '/tacotron2-internal/')
sys.path.append(getPath () + '/tacotron2-internal/waveglow/')

import numpy as np
import torch
from hparams import create_hparams
from model import Tacotron2
from layers import TacotronSTFT
from audio_processing import griffin_lim
from text import text_to_sequence
from denoiser import Denoiser
from unidecode import unidecode
import librosa

def ARPA(text):
	out = ''
	for word_ in text.split(" "):
		word=word_; end_chars = ''
		while any(elem in word for elem in r"!?,.;") and len(word) > 1:
			if word[-1] == '!': end_chars = '!' + end_chars; word = word[:-1]
			if word[-1] == '?': end_chars = '?' + end_chars; word = word[:-1]
			if word[-1] == ',': end_chars = ',' + end_chars; word = word[:-1]
			if word[-1] == '.': end_chars = '.' + end_chars; word = word[:-1]
			if word[-1] == ';': end_chars = ';' + end_chars; word = word[:-1]
			else: break
		try: word_arpa = ARPADict[word.upper()]
		except: word_arpa = ''
		if len(word_arpa)!=0: word = "{" + str(word_arpa) + "}"
		out = (out + " " + word + end_chars).strip()
	if out[-1] != "␤": out = out + "␤"
	return out

def createModel (filename):
	model = Tacotron2(hparams)
	model.load_state_dict(torch.load(filename)['state_dict'])
	_ = model.cuda().eval().half()
	return model

ARPADict = {}
sigma = 0.75
denoise_strength = 0.01
raw_input_ = False  # disables automatic ARPAbet conversion, useful for inputting your own pronounciation or just for testing 

personalizedModels = {}
lastUsedModel = ""

def initialize ():
	global hparams, waveglow, model, denoiser, models, availableModelNames

	for line in reversed((open(getPath () + '/phonemes.txt', "r").read()).splitlines()):
		ARPADict[(line.split(" ",1))[0]] = (line.split(" ",1))[1].strip()

	# Setup Parameters
	hparams = create_hparams()
	hparams.sampling_rate = 48000
	hparams.max_decoder_steps = 3000 # how many steps before cutting off generation, too many and you may get CUDA errors.
	hparams.gate_threshold = 0.30 # Model must be 30% sure the clip is over before ending generation

	print ("Loading /tacotron/waveglow.pt file...")

	if (not os.path.isfile (getPath () + '/waveglow.pt')):
		print ("Please download https://drive.google.com/open?id=1DMyL3RxFqAVhH60VCLnVaDt2YJb2RCfz, rename it to waveglow.pt and put it in the tacotron folder")
		exit (1)

	# Load WaveGlow model into GPU
	waveglow_pretrained_model = getPath () + '/waveglow.pt'
	waveglow = torch.load(waveglow_pretrained_model)['model']
	waveglow.cuda().eval().half()
	for k in waveglow.convinv:
		k.float()
	denoiser = Denoiser(waveglow)
	print ("This WaveGlow model has been trained for ", torch.load(waveglow_pretrained_model)['iteration'], " Iterations.")

	models = {}
	availableModelNames = []

	print ("Loading tacotron voices from /tacotron/voices ...")
	# Load Tacotron2 model into GPU
	for model in os.listdir (getPath () + "/voices"):
		if (model[0] == "."): continue
		availableModelNames.append (model.lower ())
		print ("Found TTS model: " + model.lower ())
		models[model.lower ()] = createModel (getPath () + '/voices/' + model)

	#print("This Tacotron model has been trained for ",torch.load(tacotron2_pretrained_model)['iteration']," Iterations.")

def generateWAV (message, authorID, outputFile):
	global lastUsedModel
	
	message = unidecode (message)
	if len(message) < 1: return

	if (message[0] == '`'): message = message[1:] + "␤"
	else: message = ARPA (message + ".")

	print ("Generating message: " + message)
	
	if (authorID in personalizedModels): modelName = personalizedModels[authorID]
	else: modelName = availableModelNames[0]
	if (lastUsedModel != modelName): torch.cuda.empty_cache()
	lastUsedModel = modelName
	model = models[modelName]

	with torch.no_grad():
		sequence = np.array(text_to_sequence(message, ['english_cleaners']))[None, :]
		sequence = torch.autograd.Variable(torch.from_numpy(sequence)).cuda().long()
		mel_outputs, mel_outputs_postnet, _, alignments = model.inference(sequence)

		audio = waveglow.infer(mel_outputs_postnet, sigma=sigma);
		audio_denoised = denoiser(audio, strength=denoise_strength)[:, 0];
		librosa.output.write_wav(outputFile, np.swapaxes(audio_denoised.cpu().numpy(),0,1), hparams.sampling_rate, norm=True)

def updateAuthorModel (authorID, model):
	availableVoices = []

	for testModel in availableModelNames:
		availableVoices.append ("`{}`".format (testModel))
		if (testModel.startswith (model) and len (model) > 0):
			personalizedModels[authorID] = testModel
			return "Voice updated as: **{}**".format (testModel)
	
	return "Invalid voice. Availble voices are:\n{}".format (", ".join (availableVoices))